<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'visiteurs_connectes_description' => '-* Displays the number of visitors connected on the site.
-* Add <code>&#60;INCLURE{fond=inc-visiteurs}&#62;</code> in your pages.',
	'visiteurs_connectes_nom' => 'Number of visitors connected',
	'visiteurs_connectes_slogan' => '',
);
