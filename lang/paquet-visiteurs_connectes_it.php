<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'visiteurs_connectes_description' => '-* Visualizza il numero di visitatori collegati sul sito.
-* Aggiungere <code>&#60;INCLURE{fond=inc-visiteurs}&#62;</code> nelle vostre pagine.',
	'visiteurs_connectes_nom' => 'Numero di visitatori collegati',
	'visiteurs_connectes_slogan' => '',
);

