<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'visiteurs_connectes_description' => '-* Zeigt die anzahl der besucher auf der website verbunden.
-* hinzuf&#252;gen <code>&#60;INCLURE{fond=inc-visiteurs}&#62;</code> in ihre seiten.',
	'visiteurs_connectes_nom' => 'Anzahl der Besucher verbunden',
	'visiteurs_connectes_slogan' => '',
);
