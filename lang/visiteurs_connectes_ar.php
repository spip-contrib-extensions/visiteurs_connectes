<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
 	// Bare de nav
	'visiteurs_en_ce_moment' => '@nb@ &#1605;&#1606; &#1575;&#1604;&#1586;&#1608;&#1575;&#1585; &#1575;&#1604;&#1570;&#1606;'
);
