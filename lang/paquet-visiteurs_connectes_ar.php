<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'visiteurs_connectes_description' => '-* &#1610;&#1593;&#1585;&#1590; &#1593;&#1583;&#1583; &#1575;&#1604;&#1586;&#1608;&#1575;&#1585; &#1593;&#1604;&#1609; &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;.
-* &#1571;&#1590;&#1610;&#1601; <code>&#60;INCLURE{fond=inc-visiteurs}&#62;</code> &#1601;&#1610; &#1589;&#1601;&#1581;&#1575;&#1578;&#1603;.',
	'visiteurs_connectes_nom' => '&#1593;&#1583;&#1583; &#1575;&#1604;&#1586;&#1608;&#1575;&#1585; &#1575;&#1604;&#1605;&#1578;&#1589;&#1604;&#1608;&#1606;',
	'visiteurs_connectes_slogan' => '',
);
