<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'visiteurs_connectes_description' => '-* Un plugin gadget qui permet d’afficher le nombre de visites en ce moment sur votre site.
-* Ajouter <code>&#60;INCLURE{fond=inc-visiteurs}&#62;</code> dans vos pages.',
	'visiteurs_connectes_nom' => 'Nombre de visites en ce moment',
	'visiteurs_connectes_slogan' => 'Affiche le nombre de visites en ce moment sur le site',
);
