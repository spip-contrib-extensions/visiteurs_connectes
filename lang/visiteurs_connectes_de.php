<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
 	// Bare de nav
	'visiteurs_en_ce_moment' => '@nb@ besucheranzahl heute'
);
