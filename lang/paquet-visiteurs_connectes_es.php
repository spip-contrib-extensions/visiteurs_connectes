<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'visiteurs_connectes_description' => '-* Muestra el n&#250;mero de visitantes conectados en el sitio.
-* Agregar <code>&#60;INCLURE{fond=inc-visiteurs}&#62;</code> en tus p&#225;ginas.',
	'visiteurs_connectes_nom' => 'N&#250;mero de visitantes conectados',
	'visiteurs_connectes_slogan' => '',
);
