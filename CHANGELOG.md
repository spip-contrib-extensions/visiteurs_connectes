# Changelog

## 2.0.4 - 2024-04-12

### Fixed
- #1 Utiliser une formulation épicène (écriture inclusive)
- Sécurité et ne pas fermer les fichiers PHP de langue

### Added
- Ajout d'un CHANGELOG.md et d'un README.md

## 2.0.3 2023-08-27

### Changed
- Mise à jour des bornes de compatibilité pour SPIP 4.2
- Passage en stable

## 2.0.2 2022-06-08

### Added
- Ajout d'un logo au format SVG

### Changed
- Mise à jour des bornes de compatibilité pour SPIP 4.1
